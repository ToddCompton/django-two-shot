# Generated by Django 4.1.3 on 2022-12-03 02:49

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("receipts", "0001_initial"),
    ]

    operations = [
        migrations.AlterModelOptions(
            name="receipt",
            options={"ordering": ["date"]},
        ),
    ]
