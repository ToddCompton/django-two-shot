from django.shortcuts import render, get_object_or_404, redirect
from receipts.models import ExpenseCategory, Account, Receipt
from django.forms import ModelForm
from django.contrib.auth.decorators import login_required
from django.contrib.auth import login, authenticate, logout
from receipts.forms import ReceiptForm, ExpenseCategoryForm, AccountForm
from django.contrib.auth.models import User

# Create your views here.


@login_required
def home(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    context = {
        "home": receipts,
    }
    return render(request, "home.html", context)


@login_required()
def create_receipt(request):
    if request.method == "POST":
        # We should use the form to validate the values
        # and save them to the database
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            # added from.save
            form.save()
            # If all goes well, we can redirect the browser
            # to another page and leave the function
            return redirect("home")
    else:
        # Create and instance of the Django model form class
        form = ReceiptForm()

    # Put the form in the context
    context = {
        "form": form,
    }

    return render(request, "create.html", context)


@login_required()
def category_list(request):
    categories = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "category_list": categories,
    }
    return render(request, "categories.html", context)


@login_required()
def account_list(request):
    accounts = Account.objects.filter(owner=request.user)
    context = {
        "account_list": accounts,
    }
    return render(request, "accounts.html", context)


@login_required()
def create_category(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            category = form.save(False)
            category.owner = request.user
            category = form.save()
            return redirect("category_list")
    else:
        form = ExpenseCategoryForm()
    context = {
        "form": form,
    }
    return render(request, "categories/create.html", context)


@login_required()
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.owner = request.user
            account = form.save()
            return redirect("account_list")
    else:
        form = AccountForm()
    context = {
        "form": form,
    }
    return render(request, "accounts/create.html", context)
